﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public static Transform ins;

    public Sprite Idle;
    public Sprite Jump;

    private Sprite Neutral;

    public Sprite Walking1;
    public Sprite Walking2;
    public Sprite Walking3;
    private Sprite Walking4;

    private Sprite Walk;
    private float WalkingTimer = 0;

    [Space]

    public Sprite Block;
    public Sprite Light;
    public Sprite Heavy;

    private int AttackType = 0;
    private float AttackAstTimer = 0;

    [Space]

    public float Zmin;
    public float Zmax;
    public float Xmin;
    public float Xmax;

    public float Speed;
    public float JumpForce;

    public Rigidbody rb;
    public Transform Floor;

    public Combo Base;
    private Combo Current;

    public float AttackDistance;
    public float HAttackTime;
    public float LAttackTime;
    public float AttackTimer = 0;
    public float ComboMax;
    public float ComboTimer = 0;

    Transform Target;

    private bool Air = false;

    private void Start()
    {
        Current = Base;
        ins = transform;
        Walking4 = Walking2;
    }

    void Update ()
    {
        Camera.main.transform.position = new Vector3(transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);

        if (Input.GetButton("Up") && transform.position.z < Zmax)
        {
            //rb.AddForce(new Vector3(0, Speed * Time.deltaTime, Speed * Time.deltaTime));
            transform.parent.position += new Vector3(0,Speed * Time.deltaTime, Speed * Time.deltaTime);
        }
        else if (Input.GetButton("Down") && transform.position.z > Zmin)
        {
            transform.parent.position -= new Vector3(0, Speed * Time.deltaTime, Speed * Time.deltaTime);
        }

        if (Input.GetButton("Left") && transform.position.x > Xmin)
        {
            transform.parent.position -= new Vector3(Speed * Time.deltaTime, 0, 0);
            transform.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else if (Input.GetButton("Right") && transform.position.x < Xmax)
        {
            transform.parent.position += new Vector3(Speed * Time.deltaTime, 0, 0);
            transform.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        if (Air || (!Input.GetButton("Up") && !Input.GetButton("Down") && !Input.GetButton("Left") && !Input.GetButton("Right")))
        {
            transform.GetComponent<SpriteRenderer>().sprite = Neutral;
        }
        else if (Input.GetButton("Up") || Input.GetButton("Down") || Input.GetButton("Left") || Input.GetButton("Right"))
        {
            transform.GetComponent<SpriteRenderer>().sprite = Walk;
            if (WalkingTimer <= 0)
            {
                WalkingTimer = 0.3f;
                if (Walk == Walking1)
                {
                    Walk = Walking2;
                }
                else if (Walk == Walking2)
                {
                    Walk = Walking3;
                }
                else if (Walk == Walking3)
                {
                    Walk = Walking4;
                }
                else
                {
                    Walk = Walking1;
                }
            }
            else
            {
                WalkingTimer -= Time.deltaTime;
            }
        }
        if (AttackTimer > 0)
        {
            AttackTimer -= Time.deltaTime;
            if (AttackAstTimer > 0)
            {
                AttackAstTimer -= Time.deltaTime;
                switch (AttackType)
                {
                    case 0:
                        transform.GetComponent<SpriteRenderer>().sprite = Heavy;
                        break;
                    case 1:
                        transform.GetComponent<SpriteRenderer>().sprite = Light;
                        break;
                    case 2:
                        transform.GetComponent<SpriteRenderer>().sprite = Block;
                        break;

                }
            }
        }
        else if (Input.GetButton("Light"))
        {
            AttackType = 1;
            AttackAstTimer = LAttackTime / 4;
            transform.GetComponent<SpriteRenderer>().sprite = Light;
        }
        else if (Input.GetButton("Heavy"))
        {
            AttackType = 0;
            AttackAstTimer = LAttackTime / 4;
            transform.GetComponent<SpriteRenderer>().sprite = Heavy;
        }
        else if (Input.GetButton("Block"))
        {
            AttackType = 2;
            AttackAstTimer = LAttackTime / 4;
            transform.GetComponent<SpriteRenderer>().sprite = Block;
        }

        if (Input.GetButtonDown("Jump") && !Air)
        {
            rb.AddForce(new Vector2(0, JumpForce), ForceMode.Impulse);
        }

        if (ComboTimer > 0)
        {
            ComboTimer -= Time.deltaTime;
        }
        if (AttackTimer <= 0)
        {
            Attack();
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform == Floor)
        {
            Air = false;
            Neutral = Idle;
        }
        else
        {

            RaycastHit hit;
            if (collision.transform.GetComponent<Rigidbody>() && Physics.Raycast(transform.position, -transform.up, out hit, AttackDistance) && hit.transform == collision.transform)
            {
                Target = collision.transform;
                rb.AddForce(new Vector2(0, JumpForce / 4), ForceMode.Impulse);
                Target.GetComponent<Rigidbody>().AddForce(transform.right * (JumpForce / 2), ForceMode.Impulse);
                Target.GetComponent<DamageManager>().TakeDamage(10);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform == Floor)
        {
            Air = true;
            Neutral = Jump;
        }
    }

    private void Attack()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position,transform.right, out hit, AttackDistance))
        {
            Debug.DrawLine(transform.position, transform.position + (transform.right * AttackDistance), Color.green);
            if (hit.transform.tag == "Enemy")
            {
                Target = hit.transform;
                if (Input.GetButtonDown("Light") || Input.GetButtonDown("Heavy") || Input.GetButtonDown("Block"))
                {
                    if (ComboTimer <= 0)
                    {
                        print("reset");
                        Current = Base;
                    }
                    PressAttack();
                }
            }
        }
        else
        {
            Debug.DrawLine(transform.position, transform.position + (transform.right * AttackDistance), Color.red);
        }
    }

    int NoCombo = 0;

    private void PressAttack()
    {
        if (Input.GetButtonDown("Light"))
        {
            Current = Current.Light[0];
        }
        else if(Input.GetButtonDown("Heavy"))
        {
            Current = Current.Heavy[0];
        }
        else if(Input.GetButtonDown("Block"))
        {
            Current = Current.Block[0];
        }
        
        Target.GetComponent<DamageManager>().TakeDamage(Current.Effect.Damage);
        if (Current.Effect.Raise)
        {
            Target.GetComponent<Rigidbody>().AddForce(new Vector2(0, JumpForce/2), ForceMode.Impulse);
        }
        if (Current.Effect.Throw)
        {
            Target.GetComponent<Rigidbody>().AddForce(transform.right*(JumpForce / 2), ForceMode.Impulse);
        }
        if (Current.Effect.Jump)
        {
            rb.AddForce(new Vector2(0, JumpForce/2), ForceMode.Impulse);
        }
        if (Current.Effect.Lock)
        {
            Target.GetComponent<AI>().AttackTimer += HAttackTime*2;
        }
        if (Current.Effect.Final)
        {
            AttackTimer = LAttackTime;
            NoCombo = 0;
            Current = Base;
        }
        else
        {
            ComboTimer = ComboMax / ++NoCombo ;
        }
        
    }
}

[System.Serializable]
public class Combo
{
    public Combo[] Block;
    public Combo[] Light;
    public Combo[] Heavy;

    public Attack Effect;
}

[System.Serializable]
public class Attack
{
    public float Damage;
    public bool Raise;
    public bool Throw;
    public bool Jump;
    public bool Lock;
    public bool Final;
}