﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageMove : MonoBehaviour {

    public float Lifespan = 2;
    public float MoveSpeed = 2;
    public float VisLifespan = 0.1f;
    public float Visibilty = 1;

    public Text t;

    void Update ()
    {
        transform.position += new Vector3(0, MoveSpeed * Time.deltaTime);
        if (Lifespan > 0)
        {
            Lifespan -= Time.deltaTime;
        }
        else
        {
            t.color = new Color(255, 255, 255, Visibilty);
            Visibilty -= VisLifespan;
            if (Visibilty <= 0)
            {
                Destroy(gameObject);
            }
        }
	}
}
