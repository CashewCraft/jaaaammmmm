﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow : MonoBehaviour {

    public Transform Of;
    public SpriteRenderer Me;
	
	void Update ()
    {
        Me.color = new Color(0,0,0,Mathf.Max(0.05f,1-(1/(Of.position.y - transform.position.y))));
	}
}
