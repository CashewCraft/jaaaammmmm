﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageManager : MonoBehaviour {

    public float Health;
    private float MaxHealth;
    private float ThreshHold;
    public Rigidbody rb;

    public Transform Floor;

    public bool IsAi;
    public AI me;

    public void Start()
    {
        MaxHealth = Health;
    }

    public void Update()
    {
        Floor.position = new Vector3(transform.position.x, Floor.position.y, Floor.position.z);
    }

    public void TakeDamage(float Amount)
    {
        DamageCreator.ins.MakeDamage(Amount, transform.position);
        Health -= Amount;

        if (Health <= 0)
        {
            EnemySpawning.Enemies.Remove(me);
            Destroy(transform.parent.gameObject);
        }
        else if (IsAi)
        {
            ThreshHold += Amount;
            if (ThreshHold > MaxHealth * 0.2f)
            {
                me.Kill = false;
            }
        }
    }
}
