﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageCreator : MonoBehaviour {

    public RectTransform Prefab;
    public GameObject From;

    public static DamageCreator ins;

    private void Start()
    {
        ins = this;
    }

    void Update ()
    {
		if (Input.GetKeyDown(KeyCode.Q))
        {
            MakeDamage(Random.Range(50, 400), From.transform.position);
        }
	}

    public void MakeDamage(float Amount, Vector3 Pos)
    {
        RectTransform Dmg = Instantiate(Prefab);
        Dmg.SetParent(transform, false);
        Dmg.position = Camera.main.WorldToScreenPoint(Pos);
        Dmg.GetComponent<Text>().text = Amount.ToString();
    }
}
